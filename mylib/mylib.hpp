#pragma once

extern "C" {

extern void __libc_init_array(void);
extern void __cxa_pure_virtual(void);

}

namespace mylib {

template <typename T>
class ptr {
  T *xPtr;
  ptr(const ptr &);
  ptr &operator =(const ptr &);
  public:
    ptr(T *t = 0) : xPtr(t) {}
    ~ptr() {delete xPtr;}
    void reset(T *p=0) {delete xPtr; xPtr = p;}
    operator bool() {return xPtr == 0;}
    T *get() {return xPtr;}
    T *operator ->() {return xPtr;}
    T &operator *() {return *xPtr;}
};


template <typename N, unsigned I>
class Allocatable {
  static char xMem[];
  static char xAllocMap[];
  public:
    void *operator new(unsigned) throw() {
      for (unsigned j=0u; j<(I-1u)/8u+1u; ++j) {
        for (unsigned char k=0u; k<8u; ++k) {
          if ((j*8 + k) >= I) return 0;
          const unsigned char mask = (1u << k);
          if ((xAllocMap[j] & mask) == 0) {
            xAllocMap[j] |= mask;
            return xMem + (j*8 + k)*sizeof(N);
          }
        }
      }
      return 0;
    }

    void operator delete(void *p) {
      for (unsigned j=0u; j<I; ++j) {
        if (p == static_cast <void *> (xMem + j*sizeof(N))) {
          const unsigned a_index = j/8u;
          const unsigned char mask = (1u << (j/8u % 8u));
          xAllocMap[a_index] &= ~mask;
        }
      }
    }
};

template <typename N, unsigned I>
char Allocatable<N,I>::xMem[I*sizeof(N)] = {0};

template <typename N, unsigned I>
char Allocatable<N,I>::xAllocMap[(I-1)/8+1] = {0};


/*
class C : public Allocatable <C, 2> {
  public:
};
*/


} // mylib
