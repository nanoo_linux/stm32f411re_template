#include "mylib.hpp"

extern "C" {

void __libc_init_array(void) {
  extern void (*__init_array_start[])(void);
  extern void (*__init_array_end[])(void);
  for (unsigned j=0; __init_array_start[j] != __init_array_end[0]; ++j) {
    __init_array_start[j]();
  }
}

void __cxa_pure_virtual(void) {
  while(1) {}
}

}
