.PHONY=clean program

OBJS = tmp/system_stm32f4xx.o tmp/startup_stm32f4xx.o
OBJS += tmp/misc.o
#~ OBJS += tmp/stm32f4xx_adc.o
#~ OBJS += tmp/stm32f4xx_can.o
#~ OBJS += tmp/stm32f4xx_crc.o
#~ OBJS += tmp/stm32f4xx_cryp_aes.o
#~ OBJS += tmp/stm32f4xx_cryp.o
#~ OBJS += tmp/stm32f4xx_cryp_des.o
#~ OBJS += tmp/stm32f4xx_cryp_tdes.o
#~ OBJS += tmp/stm32f4xx_dac.o
#~ OBJS += tmp/stm32f4xx_dbgmcu.o
#~ OBJS += tmp/stm32f4xx_dcmi.o
#~ OBJS += tmp/stm32f4xx_dma.o
#~ OBJS += tmp/stm32f4xx_exti.o
#~ OBJS += tmp/stm32f4xx_flash.o
#~ OBJS += tmp/stm32f4xx_fsmc.o
OBJS += tmp/stm32f4xx_gpio.o
#~ OBJS += tmp/stm32f4xx_hash.o
#~ OBJS += tmp/stm32f4xx_hash_md5.o
#~ OBJS += tmp/stm32f4xx_hash_sha1.o
#~ OBJS += tmp/stm32f4xx_i2c.o
#~ OBJS += tmp/stm32f4xx_iwdg.o
#~ OBJS += tmp/stm32f4xx_pwr.o
OBJS += tmp/stm32f4xx_rcc.o
#~ OBJS += tmp/stm32f4xx_rng.o
#~ OBJS += tmp/stm32f4xx_rtc.o
#~ OBJS += tmp/stm32f4xx_sdio.o
#~ OBJS += tmp/stm32f4xx_spi.o
#~ OBJS += tmp/stm32f4xx_syscfg.o
OBJS += tmp/stm32f4xx_tim.o
#~ OBJS += tmp/stm32f4xx_usart.o
#~ OBJS += tmp/stm32f4xx_wwdg.o
OBJS += tmp/mylib.o

# add here objects
OBJS += tmp/main.o


#~ CROSS= $(HOME)/apps/git/stm32/gcc-arm-none-eabi-5_2-2015q4/bin/arm-none-eabi-
CROSS = arm-none-eabi-
CC = $(CROSS)gcc
CXX = $(CROSS)g++
OBJCOPY = $(CROSS)objcopy

CXXFLAGS  = -Os -Wall -fno-exceptions -fno-rtti -fno-use-cxa_atexit -std=gnu++0x
HARDFP =
ifdef HARDFP
	CXXFLAGS += -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork
	CXXFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
else
	CXXFLAGS += -mthumb -mcpu=cortex-m4
	CXXFLAGS += -mfloat-abi=soft
endif
# TODO: hard float was causing an exception; see what's up.
LDFLAGS = -Wl,-Map,tmp/main.map -g -Tbsp/stm32f4_flash.ld -specs=nosys.specs -nostdlib
INCPATH += -Isrc -Ibsp -Ilibraries/STM32F4xx_StdPeriph_Driver/inc -Ilibraries/CMSIS/ST/STM32F4xx/Include -Ilibraries/CMSIS/Include

main.bin: tmp/main.elf
	$(OBJCOPY) -O binary $^ $@

tmp/system_stm32f4xx.o: bsp/system_stm32f4xx.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/startup_stm32f4xx.o: bsp/startup_stm32f4xx.s
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/misc.o: libraries/STM32F4xx_StdPeriph_Driver/src/misc.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_adc.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_can.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_crc.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_cryp_aes.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_cryp.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_cryp_des.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_cryp_tdes.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_dac.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_dbgmcu.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_dcmi.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_dma.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_exti.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_flash.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_fsmc.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_gpio.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_hash.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_hash_md5.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_hash_sha1.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_i2c.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_iwdg.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_pwr.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_rcc.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_rng.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_rtc.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_sdio.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_spi.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_syscfg.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_tim.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_usart.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/stm32f4xx_wwdg.o: libraries/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.c
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $^

tmp/mylib.o: mylib/mylib.cpp mylib/mylib.hpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $<

tmp/main.elf: $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

clean:
	rm -f tmp/* main.bin

program: main.bin
	sudo st-flash --reset write main.bin 0x8000000


# add here sources and dependencies
tmp/main.o: src/main.cpp src/timer_blink.hpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $<
