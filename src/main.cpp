#if 0
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>

class Gpio {
  static void ms_delay(int);
  Gpio();
  public:
    static Gpio &getInstance();
    static void toggle();
    static void delay();
};

Gpio::Gpio() {
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  GPIO_InitTypeDef p;
  GPIO_StructInit(&p);
  p.GPIO_Pin = GPIO_Pin_5;
  p.GPIO_Mode = GPIO_Mode_OUT;
  p.GPIO_Speed = GPIO_Speed_100MHz;
  p.GPIO_OType = GPIO_OType_PP;
  p.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &p);
}

void Gpio::toggle() {
  GPIO_ToggleBits(GPIOA, GPIO_Pin_5);
}

void Gpio::delay() {
  ms_delay(1000);
}

Gpio &
Gpio::getInstance() {
  static Gpio gpio = Gpio();
  return gpio;
}


void Gpio::ms_delay(int ms) {
  while (ms-- > 0) {
    volatile int x = 6450;
    while (x-- > 0) {
      __asm("nop");
    }
  }
}


int main() {
  while(1) {
    Gpio &g = Gpio::getInstance();
    g.toggle();
    g.delay();
  }
}
#elif 1
#include "timer_blink.hpp"
#else

extern "C" {

void __libc_init_array(void) {
  extern void (*__init_array_start[])();
  extern void (*__init_array_end[])();
  for (unsigned j=0; __init_array_start[j] != __init_array_end[0]; ++j) {
    __init_array_start[j]();
  }
}

}


int main()
{
}

#endif
