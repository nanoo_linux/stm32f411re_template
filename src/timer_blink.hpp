#pragma once

#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <misc.h>

#if 1

extern "C" {

void __libc_init_array(void) {
  extern void (*__init_array_start[])(void);
  extern void (*__init_array_end[])(void);
  for (unsigned j=0; __init_array_start[j] != __init_array_end[0]; ++j) {
    __init_array_start[j]();
  }
}


extern char *end;

}

class Blink {
  public:
    Blink();
    void toggle();
} blink = Blink();

class Timer {
  public:
    Timer();
} timer = Timer();


extern "C" {

void TIM2_IRQHandler() {
  if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) {
    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
    blink.toggle();
  }
}

}

Blink::Blink() {
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  GPIO_InitTypeDef p;
  GPIO_StructInit(&p);
  p.GPIO_Pin = GPIO_Pin_5;
  p.GPIO_Mode = GPIO_Mode_OUT;
  p.GPIO_Speed = GPIO_Speed_2MHz;
  //p.GPIO_OType = GPIO_OType_PP;
  //p.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &p);
}

void
Blink::toggle() {
  GPIO_ToggleBits(GPIOA, GPIO_Pin_5);
}

Timer::Timer() {
	TIM_TimeBaseInitTypeDef TIM_BaseStruct;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_BaseStruct.TIM_Prescaler = 50000;
  TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_BaseStruct.TIM_Period = 1000;
  TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_BaseStruct.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM2, &TIM_BaseStruct);
  TIM_Cmd(TIM2, ENABLE);
  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

  NVIC_InitTypeDef nvicStructure;
  nvicStructure.NVIC_IRQChannel = TIM2_IRQn;
  nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
  nvicStructure.NVIC_IRQChannelSubPriority = 1;
  nvicStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nvicStructure);
}

int main() {
  while (true) {
    __WFI();
  }
}
#else

/*
extern "C" {

void __libc_init_array(void) {
  extern void (*__init_array_start[])();
  extern void (*__init_array_end[])();
  for (unsigned j=0; __init_array_start[j] != __init_array_end[0]; ++j) {
    __init_array_start[j]();
  }
}

void __aeabi_atexit() {}

void *__dso_handle = 0;

}*/

extern "C" {

void TIM2_IRQHandler() {
  if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) {
    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
    GPIO_ToggleBits(GPIOA, GPIO_Pin_5);
  }
}

}

void blink_init() {
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  GPIO_InitTypeDef p;
  GPIO_StructInit(&p);
  p.GPIO_Pin = GPIO_Pin_5;
  p.GPIO_Mode = GPIO_Mode_OUT;
  p.GPIO_Speed = GPIO_Speed_50MHz;
  //p.GPIO_OType = GPIO_OType_PP;
  //p.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &p);
}

void timer_init() {
	TIM_TimeBaseInitTypeDef TIM_BaseStruct;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_BaseStruct.TIM_Prescaler = 50000;
  TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_BaseStruct.TIM_Period = 1000;
  TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_BaseStruct.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM2, &TIM_BaseStruct);
  TIM_Cmd(TIM2, ENABLE);
  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

  NVIC_InitTypeDef nvicStructure;
  nvicStructure.NVIC_IRQChannel = TIM2_IRQn;
  nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
  nvicStructure.NVIC_IRQChannelSubPriority = 1;
  nvicStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nvicStructure);
}

struct A {
  A() {}
  ~A() {}
} a = A();
int main() {
  blink_init();
  timer_init();
  //int *i = new int;
  //delete i;
  while(true) {
    __WFI();
  }
}

#endif
