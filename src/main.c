#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>

void gpio_init() {
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  GPIO_InitTypeDef p;
  GPIO_StructInit(&p);
  p.GPIO_Pin = GPIO_Pin_5;
  p.GPIO_Mode = GPIO_Mode_OUT;
  p.GPIO_Speed = GPIO_Speed_100MHz;
  p.GPIO_OType = GPIO_OType_PP;
  p.GPIO_PuPd = GPIO_PuPd_UP;

  GPIO_Init(GPIOA, &p);
}


void ms_delay(int ms) {
  while (ms-- > 0) {
    volatile int x = 6450;
    while (x-- > 0) {
      __asm("nop");
    }
  }
}


int main() {
  gpio_init();
  while(1) {
    GPIO_ToggleBits(GPIOA, GPIO_Pin_5);
    ms_delay(1000);
  }
}
